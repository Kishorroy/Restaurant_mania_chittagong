package info.androidhive.cardview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;

public class crush_finlay_location extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crush_finlay_location);
        WebView view=(WebView)findViewById(R.id.webView);
        String url="https://www.google.com.bd/maps/place/Crush+cafe,+Finaly+Square/@22.3663622,91.8220199,17z/data=!3m1!4b1!4m5!3m4!1s0x30acd8866407b95d:0xd8a9e556a6c7dfb3!8m2!3d22.3663573!4d91.8242086?hl=en";
        view.getSettings().setJavaScriptEnabled(true);
        view.loadUrl(url);
    }
}
