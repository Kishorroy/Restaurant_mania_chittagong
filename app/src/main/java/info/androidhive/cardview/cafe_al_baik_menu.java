package info.androidhive.cardview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;

public class cafe_al_baik_menu extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cafe_al_baik_menu);
        WebView view=(WebView)findViewById(R.id.webView8);
        String url="https://samirchy.000webhostapp.com/res/view/cafe_al_baik.html";
        view.getSettings().setJavaScriptEnabled(true);
        view.loadUrl(url);
    }
}
