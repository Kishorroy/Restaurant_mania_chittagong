package info.androidhive.cardview;

/**
 * Created by kishor on 9/5/2017.
 */
import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;

public class splash extends Activity {
    private boolean backbtnpress;
    private static final int SPLASH_DURATION=5000;
    private Handler myhandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_splash);
        myhandler=new Handler();
        myhandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                finish();
                if(!backbtnpress)
                {
                    Intent intent=new Intent(splash.this,MainActivity.class);
                    splash.this.startActivity(intent);
                }
            }
        },SPLASH_DURATION);

    }

    @Override
    public void onBackPressed() {
        backbtnpress=true;
        super.onBackPressed();
    }
}
