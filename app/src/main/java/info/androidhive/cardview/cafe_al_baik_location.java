package info.androidhive.cardview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;

public class cafe_al_baik_location extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cafe_al_baik_location);
        WebView view=(WebView)findViewById(R.id.webView7);
        String url="https://www.google.com.bd/maps/place/Cafe+Al+Baik/@22.3662144,91.8221721,17z/data=!3m1!4b1!4m5!3m4!1s0x30acd88663aa933b:0xa656f9eff268401f!8m2!3d22.3662144!4d91.8243608?hl=en";
        view.getSettings().setJavaScriptEnabled(true);
        view.loadUrl(url);
    }
}
