package info.androidhive.cardview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;

public class chottometro_location extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chottometro_location);
        WebView view=(WebView)findViewById(R.id.webView12);
        String url="https://www.google.com.bd/maps/search/chattometro+finley+square/@22.3480854,91.8153364,14z/data=!3m1!4b1?hl=en";
        view.getSettings().setJavaScriptEnabled(true);
        view.loadUrl(url);
    }
}
