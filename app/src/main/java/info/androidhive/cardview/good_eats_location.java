package info.androidhive.cardview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;

public class good_eats_location extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_good_eats_location);
        WebView view=(WebView)findViewById(R.id.webView15);
        String url="https://www.google.com.bd/maps/place/Good+Eats/@22.3662327,91.8221781,17z/data=!4m8!1m2!2m1!1sgood+eats+finley+square!3m4!1s0x30acd88662529699:0x1fb3a1ba5b8dff44!8m2!3d22.366251!4d91.8243728?hl=en";
        view.getSettings().setJavaScriptEnabled(true);
        view.loadUrl(url);
    }
}
