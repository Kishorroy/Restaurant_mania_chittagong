package info.androidhive.cardview;



import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SearchView;


public class Zone3 extends AppCompatActivity implements SearchView.OnQueryTextListener{
    ListView lv;
    SearchView sv;
    ArrayAdapter<String> adapter;
    String[] data={"Taqdeer","The Gallery","Delhi Darbar BD","KGN","Hunger Games","Hunger Killers","Majlish","Red Chili","Rizeek",
                    "Royal Hut","Rio Coffee","Radisson Blu","SNOWVA RESTAURANT","Sub Zero","Regalo","Greedy Guts"};


    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zone3);
        lv=(ListView)findViewById(R.id.idlistview2);
        sv=(SearchView)findViewById(R.id.idsearch2);
        adapter=new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,data);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String data=(String) adapterView.getItemAtPosition(i);
                if(i==0)
                {
                    Intent s=new Intent(view.getContext(),taqdeer.class);
                    startActivity(s);
                }
                if(i==1)
                {
                    Intent s=new Intent(view.getContext(),gallery.class);
                    startActivity(s);
                }
                if(i==2)
                {
                    Intent s=new Intent(view.getContext(),delhi_darber_bd.class);
                    startActivity(s);
                }
                if(i==3)
                {
                    Intent s=new Intent(view.getContext(),kgn.class);
                    startActivity(s);
                }
                if(i==4)
                {
                    Intent s=new Intent(view.getContext(),hunger_games.class);
                    startActivity(s);
                }
                if(i==5)
                {
                    Intent s=new Intent(view.getContext(),hunger_killers.class);
                    startActivity(s);
                }
                if(i==6)
                {
                    Intent s=new Intent(view.getContext(),majlish.class);
                    startActivity(s);
                }
                if(i==7)
                {
                    Intent s=new Intent(view.getContext(),red_chili.class);
                    startActivity(s);
                }
                if(i==8)
                {
                    Intent s=new Intent(view.getContext(),rizeek.class);
                    startActivity(s);
                }
                if(i==9)
                {
                    Intent s=new Intent(view.getContext(),royal_hut.class);
                    startActivity(s);
                }
                if(i==10)
                {
                    Intent s=new Intent(view.getContext(),rio.class);
                    startActivity(s);
                }
                if(i==11)
                {
                    Intent s=new Intent(view.getContext(),radisson.class);
                    startActivity(s);
                }
                if(i==12)
                {
                    Intent s=new Intent(view.getContext(),snowva.class);
                    startActivity(s);
                }
                if(i==13)
                {
                    Intent s=new Intent(view.getContext(),sub_zero.class);
                    startActivity(s);
                }
                if(i==14)
                {
                    Intent s=new Intent(view.getContext(),regalo.class);
                    startActivity(s);
                }
                if(i==15)
                {
                    Intent s=new Intent(view.getContext(),greedy_guts.class);
                    startActivity(s);
                }

            }
        });
        sv.setOnQueryTextListener(this);

    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        String text=newText;
        adapter.getFilter().filter(newText);
        return false;
    }

}


