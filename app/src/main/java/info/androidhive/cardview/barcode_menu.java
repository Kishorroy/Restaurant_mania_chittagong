package info.androidhive.cardview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;

public class barcode_menu extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_barcode_menu);
        WebView view=(WebView)findViewById(R.id.webView2);
        String url="https://samirchy.000webhostapp.com/res/view/barcode.html";
        view.getSettings().setJavaScriptEnabled(true);
        view.loadUrl(url);
    }
}
