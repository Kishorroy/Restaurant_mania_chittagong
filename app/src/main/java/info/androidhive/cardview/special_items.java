package info.androidhive.cardview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;

public class special_items extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_special_items);
        WebView view=(WebView)findViewById(R.id.webView13);
        String url="https://samirchy.000webhostapp.com/res/view/special.html";
        view.getSettings().setJavaScriptEnabled(true);
        view.loadUrl(url);
    }
}
