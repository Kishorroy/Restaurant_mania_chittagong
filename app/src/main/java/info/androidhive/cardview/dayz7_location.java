package info.androidhive.cardview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;

public class dayz7_location extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dayz7_location);
        WebView view=(WebView)findViewById(R.id.webView6);
        String url="https://www.google.com.bd/maps/place/7dayz/@22.367003,91.8202213,17z/data=!3m1!4b1!4m5!3m4!1s0x30acd88621a8a16b:0x73e5ae8d26f4730f!8m2!3d22.367003!4d91.82241?hl=en";
        view.getSettings().setJavaScriptEnabled(true);
        view.loadUrl(url);
    }
}
