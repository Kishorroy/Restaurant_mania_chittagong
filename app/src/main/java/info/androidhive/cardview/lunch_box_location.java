package info.androidhive.cardview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;

public class lunch_box_location extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lunch_box_location);
        WebView view=(WebView)findViewById(R.id.webView10);
        String url="https://www.google.com.bd/maps/place/Lunch+Box/@22.3662674,91.8221191,17z/data=!4m8!1m2!2m1!1slunch+box+finley+square!3m4!1s0x30acd88663004749:0x73bfa056a3f9df46!8m2!3d22.3662014!4d91.8243621?hl=en";
        view.getSettings().setJavaScriptEnabled(true);
        view.loadUrl(url);
    }
}
