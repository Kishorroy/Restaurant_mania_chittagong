package info.androidhive.cardview;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class goodeats extends AppCompatActivity {
    Button btn16;
    Button btn17;
    Button btn18;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_goodeats);
        btn16=(Button)findViewById(R.id.btn16);
        btn17=(Button)findViewById(R.id.btn17);
        btn18=(Button)findViewById(R.id.btn18);
        btn16.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(goodeats.this,good_eats_menu.class);
                startActivity(i);
            }
        });
        btn17.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(goodeats.this,good_eats_location.class);
                startActivity(i);
            }
        });

    }
}
