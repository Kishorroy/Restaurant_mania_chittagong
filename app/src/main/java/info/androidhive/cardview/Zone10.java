package info.androidhive.cardview;



import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SearchView;


public class Zone10 extends AppCompatActivity implements SearchView.OnQueryTextListener{
    ListView lv;
    SearchView sv;
    ArrayAdapter<String> adapter;
    String[] data={"Burgwich Town","Boom Town","Green Chicken","MFC","Petuk No.1","Roof BBQ & Steak","The Migdolus","হ্যাশট্যাগ "};


    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zone10);
        lv=(ListView)findViewById(R.id.idlistview9);
        sv=(SearchView)findViewById(R.id.idsearch9);
        adapter=new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,data);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String data=(String) adapterView.getItemAtPosition(i);

            }
        });
        sv.setOnQueryTextListener(this);

    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        String text=newText;
        adapter.getFilter().filter(newText);
        return false;
    }

}



