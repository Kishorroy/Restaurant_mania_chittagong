package info.androidhive.cardview;



import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SearchView;


public class Zone2 extends AppCompatActivity implements SearchView.OnQueryTextListener{
    ListView lv;
    SearchView sv;
    ArrayAdapter<String> adapter;
    String[] data={"Dawat","Safran","Dum Phoonk","Think Food Cafe","ChottoMetro","Da Signature","Cube","Bismillah juice Ghor",
            "Cocoloco","Eat & Treat","Blue Ocean","Mughal Darbar"};


    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zone2);
        lv=(ListView)findViewById(R.id.idlistview1);
        sv=(SearchView)findViewById(R.id.idsearch1);
        adapter=new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,data);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String data=(String) adapterView.getItemAtPosition(i);
                if(i==0)
                {
                    Intent s=new Intent(view.getContext(),dawat.class);
                    startActivity(s);
                }
                if(i==1)
                {
                    Intent s=new Intent(view.getContext(),safran.class);
                    startActivity(s);
                }
                if(i==2)
                {
                    Intent s=new Intent(view.getContext(),dum_phoonk1.class);
                    startActivity(s);
                }
                if(i==3)
                {
                    Intent s=new Intent(view.getContext(),think_food.class);
                    startActivity(s);
                }
                if(i==4)
                {
                    Intent s=new Intent(view.getContext(),chottometro2.class);
                    startActivity(s);
                }
                if(i==5)
                {
                    Intent s=new Intent(view.getContext(),da_signature.class);
                    startActivity(s);
                }
                if(i==6)
                {
                    Intent s=new Intent(view.getContext(),cube.class);
                    startActivity(s);
                }
                if(i==7)
                {
                    Intent s=new Intent(view.getContext(),bismillah.class);
                    startActivity(s);
                }
                if(i==8)
                {
                    Intent s=new Intent(view.getContext(),cocoloco.class);
                    startActivity(s);
                }
                if(i==9)
                {
                    Intent s=new Intent(view.getContext(),eat_and_treat.class);
                    startActivity(s);
                }
                if(i==10)
                {
                    Intent s=new Intent(view.getContext(),blue_ocen.class);
                    startActivity(s);
                }
                if(i==11)
                {
                    Intent s=new Intent(view.getContext(),mughal.class);
                    startActivity(s);
                }

            }
        });
        sv.setOnQueryTextListener(this);

    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        String text=newText;
        adapter.getFilter().filter(newText);
        return false;
    }

}


