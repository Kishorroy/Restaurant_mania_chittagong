package info.androidhive.cardview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;

public class barcode_location extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_barcode_location);
        WebView view=(WebView)findViewById(R.id.webView4);
        String url="https://www.google.com.bd/maps/place/Barcode+Cafe/@22.3651841,91.8221694,17z/data=!3m1!4b1!4m5!3m4!1s0x30acd885d5225b61:0x22dd67202b908d6a!8m2!3d22.3651841!4d91.8243581?hl=en";
        view.getSettings().setJavaScriptEnabled(true);
        view.loadUrl(url);
    }
}
