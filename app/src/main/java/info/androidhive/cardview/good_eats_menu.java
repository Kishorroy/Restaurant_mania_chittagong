package info.androidhive.cardview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;

public class good_eats_menu extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_good_eats_menu);
        WebView view=(WebView)findViewById(R.id.webView14);
        String url="https://samirchy.000webhostapp.com/res/view/good_eats.html";
        view.getSettings().setJavaScriptEnabled(true);
        view.loadUrl(url);
    }
}
