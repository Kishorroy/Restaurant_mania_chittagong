package info.androidhive.cardview;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class barcode extends AppCompatActivity {
    Button btn37;
    Button btn38;
    Button btn39;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_barcode);
        btn37=(Button)findViewById(R.id.btn37);
        btn38=(Button)findViewById(R.id.btn38);
        btn39=(Button)findViewById(R.id.btn39);


        btn37.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(barcode.this,barcode_menu.class);
                startActivity(i);
            }
        });
        btn38.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(barcode.this,barcode_location.class);
                startActivity(i);
            }
        });

    }
}
