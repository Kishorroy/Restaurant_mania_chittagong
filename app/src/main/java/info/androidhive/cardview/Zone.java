package info.androidhive.cardview;



        import android.content.Intent;
        import android.os.Bundle;
        import android.support.v7.app.AppCompatActivity;
        import android.view.View;
        import android.widget.AdapterView;
        import android.widget.ArrayAdapter;
        import android.widget.ListView;
        import android.widget.SearchView;


public class Zone extends AppCompatActivity implements SearchView.OnQueryTextListener{
    ListView lv;
    SearchView sv;
    ArrayAdapter<String> adapter;
    String[] data={ "Crush Cafe","Cafe Al Baik","Lunch Box","Chotto Metro","Good Eats","Bomber & Ginger","Bonfire pizza & Grill",
                    "FaceFood","6 TEEN CAFE BISTRO","Bellpepper Restaurant","Wok & Roll","Barcode Cafe","Chicken Express BD","Barcode on Fire",
                "INDIAN DHABA SPICY","JALAPENO","7 Dayz"};


    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zone);
        lv=(ListView)findViewById(R.id.idlistview);
        sv=(SearchView)findViewById(R.id.idsearch);
        adapter=new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,data);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String data=(String) adapterView.getItemAtPosition(i);
                if(i==16)
                {
                    Intent s=new Intent(view.getContext(),dayz7.class);
                    startActivity(s);
                }
                if(i==0)
                {
                    Intent s=new Intent(view.getContext(),crushcafefinlay.class);
                    startActivity(s);
                }
                if(i==1)
                {
                    Intent s=new Intent(view.getContext(),cafealbaik.class);
                    startActivity(s);
                }
                if(i==2)
                {
                    Intent s=new Intent(view.getContext(),lunchbox.class);
                    startActivity(s);
                }
                if(i==3)
                {
                    Intent s=new Intent(view.getContext(),ChottoMetro.class);
                    startActivity(s);
                }
                if(i==4)
                {
                    Intent s=new Intent(view.getContext(),goodeats.class);
                    startActivity(s);
                }
                if(i==5)
                {
                    Intent s=new Intent(view.getContext(),bomber.class);
                    startActivity(s);
                }
                if(i==6)
                {
                    Intent s=new Intent(view.getContext(),bonfire.class);
                    startActivity(s);
                }
                if(i==7)
                {
                    Intent s=new Intent(view.getContext(),acefood.class);
                    startActivity(s);
                }
                if(i==8)
                {
                    Intent s=new Intent(view.getContext(),sixteen.class);
                    startActivity(s);
                }
                if(i==9)
                {
                    Intent s=new Intent(view.getContext(),bellpepper.class);
                    startActivity(s);
                }
                if(i==10)
                {
                    Intent s=new Intent(view.getContext(),wok.class);
                    startActivity(s);
                }
                if(i==11)
                {
                    Intent s=new Intent(view.getContext(),barcode.class);
                    startActivity(s);
                }
                if(i==12)
                {
                    Intent s=new Intent(view.getContext(),chicken_express.class);
                    startActivity(s);
                }
                if(i==13)
                {
                    Intent s=new Intent(view.getContext(),barcode_on_fire.class);
                    startActivity(s);
                }
                if(i==14)
                {
                    Intent s=new Intent(view.getContext(),dhaba.class);
                    startActivity(s);
                }
                if(i==15)
                {
                    Intent s=new Intent(view.getContext(),jalapeno.class);
                    startActivity(s);
                }


            }
        });
        sv.setOnQueryTextListener(this);

    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        String text=newText;
        adapter.getFilter().filter(newText);
        return false;
    }

}


