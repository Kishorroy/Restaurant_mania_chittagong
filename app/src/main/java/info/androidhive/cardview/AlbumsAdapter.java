package info.androidhive.cardview;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.List;

/**
 * Created by Kishor on 18/05/16.
 */
public class AlbumsAdapter extends RecyclerView.Adapter<AlbumsAdapter.MyViewHolder> {

    private Context mContext;
    private List<Album> albumList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, count;
        public ImageView thumbnail, overflow;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            count = (TextView) view.findViewById(R.id.count);
            thumbnail = (ImageView) view.findViewById(R.id.thumbnail);
            overflow = (ImageView) view.findViewById(R.id.overflow);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos=getAdapterPosition();
                    if(pos!=RecyclerView.NO_POSITION){
                        Album clickedDataItem=albumList.get(pos);
                        if(pos==0){
                        Intent intent=new Intent(mContext,Zone.class);
                        mContext.startActivity(intent);}
                        if(pos==1){
                            Intent intent=new Intent(mContext,Zone2.class);
                            mContext.startActivity(intent);
                        }
                        if(pos==2){
                            Intent intent=new Intent(mContext,Zone3.class);
                            mContext.startActivity(intent);
                        }
                        if(pos==3){
                            Intent intent=new Intent(mContext,Zone4.class);
                            mContext.startActivity(intent);
                        }
                        if(pos==4){
                            Intent intent=new Intent(mContext,Zone5.class);
                            mContext.startActivity(intent);}
                        if(pos==5){
                            Intent intent=new Intent(mContext,Zone6.class);
                            mContext.startActivity(intent);
                        }
                        if(pos==6){
                            Intent intent=new Intent(mContext,Zone7.class);
                            mContext.startActivity(intent);
                        }
                        if(pos==7){
                            Intent intent=new Intent(mContext,Zone8.class);
                            mContext.startActivity(intent);
                        }
                        if(pos==8){
                            Intent intent=new Intent(mContext,Zone9.class);
                            mContext.startActivity(intent);
                        }
                        if(pos==9){
                            Intent intent=new Intent(mContext,Zone10.class);
                            mContext.startActivity(intent);
                        }
                        if(pos==10){
                            Intent intent=new Intent(mContext,Zone11.class);
                            mContext.startActivity(intent);
                        }
                        if(pos==11){
                            Intent intent=new Intent(mContext,special_items.class);
                            mContext.startActivity(intent);
                        }
                    }
                }
            });

        }
    }


    public AlbumsAdapter(Context mContext, List<Album> albumList) {
        this.mContext = mContext;
        this.albumList = albumList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.album_card, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        Album album = albumList.get(position);
        holder.title.setText(album.getName());
        holder.count.setText( " Zone No "+album.getNumOfSongs() );

        // loading album cover using Glide library
        Glide.with(mContext).load(album.getThumbnail()).into(holder.thumbnail);

        holder.overflow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPopupMenu(holder.overflow);
            }
        });
    }

    /**
     * Showing popup menu when tapping on 3 dots
     */
    private void showPopupMenu(View view) {
        // inflate menu
        PopupMenu popup = new PopupMenu(mContext, view);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.menu_album, popup.getMenu());
        popup.setOnMenuItemClickListener(new MyMenuItemClickListener());
        popup.show();
    }

    /**
     * Click listener for popup menu items
     */
    class MyMenuItemClickListener implements PopupMenu.OnMenuItemClickListener {

        public MyMenuItemClickListener() {
        }

        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            switch (menuItem.getItemId()) {
                case R.id.action_add_favourite:
                    Toast.makeText(mContext, "Add to favourite", Toast.LENGTH_SHORT).show();
                    return true;
                case R.id.action_play_next:
                    Toast.makeText(mContext, "Play next", Toast.LENGTH_SHORT).show();
                    return true;
                default:
            }
            return false;
        }
    }

    @Override
    public int getItemCount() {
        return albumList.size();
    }
}
