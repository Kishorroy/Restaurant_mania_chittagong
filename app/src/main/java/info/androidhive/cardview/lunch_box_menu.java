package info.androidhive.cardview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;

public class lunch_box_menu extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lunch_box_menu);
        WebView view=(WebView)findViewById(R.id.webView9);
        String url="https://samirchy.000webhostapp.com/res/view/lunch_box.html";
        view.getSettings().setJavaScriptEnabled(true);
        view.loadUrl(url);
    }
    }

